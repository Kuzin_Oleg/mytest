package entity;

public class Manager {
		 
	    private String fullName; //��� ���������
	    private int Age;         //�������
	    private String Gender;   // �����
	    
	    public Manager(String fullName, int Age, String Gender) {
	        this.fullName = fullName;
	        this.Age = Age;
	        this.Gender = Gender;
	    
	    }
	 
	    public Manager() {
	    }
	 

	    public String getfullName() {
	        return fullName;
	    }
	 
	    public void setfullName(String fullName) {
	        this.fullName = fullName;
	    }
	 
	    public int getAge() {
	        return Age;
	    }
	 
	    public void setAge(int Age) {
	        this.Age = Age;
	    }
	 
	    public String getGender() {
	        return Gender;
	    }
	 
	    public void setGender(String Gender) {
	        this.Gender = Gender;
	    }

	    public String toString() {
	        return "Manager{" +
	                "fullName='" + fullName + '\'' +
	                ", Age=" + Age +
	                ", Gender=" + Gender +
	                '}';
	    }
	
}
