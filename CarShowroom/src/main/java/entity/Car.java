package entity;

import java.util.Date;

public class Car {	
	 

	    private final String Name;    			//����� ����  
	    private final String Model;       		//������ ����
	    private final Date releaseDate; 		//��� �������
	   
	 
	    public Car(String Name, Date releaseDate, String Model) {
	        this.Name = Name;
	        this.releaseDate = releaseDate;
	        this.Model = Model;
	    }
	 
	 
	    public String getName() {
	        return Name;
	    }
	 

	    public Date getreleaseDate() {
	        return releaseDate;
	        
	    }
	 
	    public String getModel() {
	        return Model;
	    }
	 
	    public String toString() {
	        return "Car{" +
	                "Name='" + Name + '\'' +
	                ", releaseDate=" + releaseDate +
	                ", Model=" + Model +
	                '}';
	    }
	
}
